import React, {useState, useEffect, useRef} from "react";
import './App.css';

function App() {
  const [messages, setMessages] = useState([]);
  const [messageText, setMessageText] = useState("");
  const [usernameText, setUsernameText] = useState("");
  const [isLoggedIn, setLoggedIn] = useState(false);

  const ws = useRef(null);

  useEffect(() => {
    ws.current = new WebSocket("ws://localhost:8000/chat");

    ws.current.onopen = () => {
      ws.current.send(JSON.stringify({type: "GET_ALL_MESSAGES"}));
    };

    ws.current.onclose = () => console.log("ws connection closed");
    ws.current.onmessage = e => {
      const decodedMessage = JSON.parse(e.data);
      if (decodedMessage.type === "NEW_MESSAGE") {
        setMessages(messages => [...messages, decodedMessage.message]);
      } else if (decodedMessage.type === "ALL_MESSAGES") {
        setMessages(messages => [...messages, ...decodedMessage.messages]);
      }
    };

    return () => ws.current.close();
  }, []);

  const changeUsername = e => {
    setUsernameText(e.target.value);
  };
  const changeMessage = e => {
    setMessageText(e.target.value);
  };

  const setUsername = e => {
    e.preventDefault();
    ws.current.send(JSON.stringify({
      type: "SET_USERNAME",
      username: usernameText
    }));
    setLoggedIn(true);
  };
  const sendMessage = e => {
    e.preventDefault();
    ws.current.send(JSON.stringify({
      type: "CREATE_MESSAGE",
      text: messageText
    }));
  };

  let chat = (
    <div>
      {
        messages.map((message, idx) => {
          return <div key={idx}>
            <b>{message.username}: </b>
            {message.text}
          </div>
        })
      }
      <form onSubmit={sendMessage}>
        <input
          type="text"
          value={messageText}
          onChange={changeMessage}
        />
        <button>Send</button>
      </form>
    </div>
  );

  if (!isLoggedIn) {
    chat = (
      <form onSubmit={setUsername}>
        <input
          type="text"
          value={usernameText}
          onChange={changeUsername}
        />
        <button>Enter chat</button>
      </form>
    );
  }

  return (
    <div className="App">
      {chat}
    </div>
  );
}

export default App;
